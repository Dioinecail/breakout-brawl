Breakout Brawl

A mobile game where you are faced against an enemy 1 vs 1, you play either as an Air Hocker player or a Breakout player and your goal is to get enemy Life meter to 0.

Air Hocker player loses lives when they receive a goal
Breakout player loses lives when a ball reaches their end of the screen