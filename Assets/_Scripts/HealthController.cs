﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Player
{
    public event Action<Player, float> onDamageReceived;
    public event Action<Player> onDied;

    public BoxCollider2D damageArea;

    private float maxHealth;
    private float currentHealth;
    private bool isDead;



    public Player(Player template, float startingHealth)
    {
        maxHealth = startingHealth;
        currentHealth = startingHealth;
        damageArea = template.damageArea;
    }

    public void TakeDamage(float amount)
    {
        currentHealth -= amount;

        onDamageReceived?.Invoke(this, currentHealth / maxHealth);

        if (currentHealth <= 0 && !isDead)
        {
            isDead = true;
            onDied?.Invoke(this);
        }
    }
}

public class HealthController : MonoBehaviour
{
    public float startingHealth;
    public float defaultDamage = 5;
    public LayerMask ballMask;

    public Player[] playerBreakoutTemplates;
    private Player[] cachedPlayersBreakout;
    private Collider2D[] buffer;



    private void Awake()
    {
        cachedPlayersBreakout = new Player[playerBreakoutTemplates.Length];

        for (int i = 0; i < cachedPlayersBreakout.Length; i++)
        {
           cachedPlayersBreakout[i] = new Player(playerBreakoutTemplates[i], startingHealth);
        }

        buffer = new Collider2D[4];
    }

    private void Update()
    {
        ClearBuffer();
        CheckDamageAreas();
    }

    private void ClearBuffer()
    {
        for (int i = 0; i < buffer.Length; i++)
        {
            buffer[i] = null;
        }
    }

    private void CheckDamageAreas()
    {
        for (int i = 0; i < cachedPlayersBreakout.Length; i++)
        {
            Physics2D.OverlapBoxNonAlloc(cachedPlayersBreakout[i].damageArea.transform.position, cachedPlayersBreakout[i].damageArea.size, 0, buffer, ballMask);

            for (int x = 0; x < buffer.Length; x++)
            {
                if (buffer[x] != null)
                {
                    BallBehaviour ball = buffer[x].GetComponent<BallBehaviour>();

                    if (ball != null)
                        cachedPlayersBreakout[x].TakeDamage(defaultDamage);

                    ball.Destroy();
                    return;
                }
            }
        }
    }
}
 