﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakoutBehaviour : MonoBehaviour
{
    public bool isControlledByCPU;
    public float movementSpeed;

    private Transform[] cachedBallTransforms;
    private float startingYPos;
    private Rigidbody2D rBody;
    private Camera mainCam;



    private void Awake()
    {
        startingYPos = transform.position.y;
        rBody = GetComponent<Rigidbody2D>();
        mainCam = Camera.main;
    }

    private void Update()
    {
        if(isControlledByCPU)
        {
            if (cachedBallTransforms == null)
                return;

            MoveByCPU();
        }
        else
            MoveByPlayer();
    }

    private void MoveByPlayer()
    {
        Vector2 mousePos = Input.mousePosition;

        Vector2 worldPos = mainCam.ScreenToWorldPoint(mousePos);
        worldPos.y = startingYPos;

        rBody.MovePosition(worldPos);
    }

    private void MoveByCPU()
    {
        Vector2 closestBallPosition = GetClosestBallPosition();

        float targetXPos = closestBallPosition.x;
        float lerpedXPos = Mathf.MoveTowards(transform.position.x, targetXPos, movementSpeed * Time.deltaTime);

        rBody.MovePosition(new Vector2(lerpedXPos, startingYPos));
    }

    private Vector2 GetClosestBallPosition()
    {
        Vector2 closestPosition = Vector2.zero;
        float minDistance = 100000;

        for (int i = 0; i < cachedBallTransforms.Length; i++)
        {
            float distance = startingYPos - cachedBallTransforms[i].position.y;

            if(distance < minDistance)
            {
                minDistance = distance;
                closestPosition = cachedBallTransforms[i].position;
            }
        }

        return closestPosition;
    }

    private void OnBallsSpawned(BallBehaviour[] balls)
    {
        cachedBallTransforms = new Transform[balls.Length];

        for (int i = 0; i < cachedBallTransforms.Length; i++)
        {
            cachedBallTransforms[i] = balls[i].transform;
        }
    }

    private void OnEnable()
    {
        GameManager.onBallsSpawned += OnBallsSpawned;
    }

    private void OnDisable()
    {
        GameManager.onBallsSpawned -= OnBallsSpawned;
    }
}