﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static event Action<BallBehaviour[]> onBallsSpawned;

    public GameObject prefabBall;

    public float spawnDelay;

    public Vector2[] positionsBall;



    private void Awake()
    {
        Invoke("SpawnBalls", spawnDelay);
    }

    private void SpawnBalls()
    {
        BallBehaviour[] balls = new BallBehaviour[positionsBall.Length];

        for (int i = 0; i < positionsBall.Length; i++)
        {
            balls[i] = Instantiate(prefabBall, positionsBall[i], Quaternion.identity).GetComponent<BallBehaviour>();
        }

        balls[1].SetPlayerBall();

        onBallsSpawned?.Invoke(balls);
    }
}