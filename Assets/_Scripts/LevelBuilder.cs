﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
    public Transform rootLevel;
    public GameObject blockPrefab;

    public int gridSizeX, gridSizeY;
    public float horizontalStep, verticalStep;



    private void Awake()
    {
        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector2 offset = new Vector2((gridSizeX) / 2f, (gridSizeY) / 2f);

                Vector2 position = new Vector2(((x - offset.x)) * horizontalStep + horizontalStep / 2f, ((y - offset.y)) * verticalStep + verticalStep / 2f);

                Instantiate(blockPrefab, position, Quaternion.identity, rootLevel);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector2 offset = new Vector2((gridSizeX) / 2f, (gridSizeY) / 2f);

                Vector2 position = new Vector2(((x - offset.x)) * horizontalStep + horizontalStep / 2f, ((y - offset.y)) * verticalStep + verticalStep / 2f);

                if(blockPrefab != null)
                    Gizmos.DrawWireCube(position, blockPrefab.transform.localScale);
                else
                    Gizmos.DrawWireCube(position, Vector3.one * 0.1f);
            }
        }
    }
}