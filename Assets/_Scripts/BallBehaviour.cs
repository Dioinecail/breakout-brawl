﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BallBehaviour : MonoBehaviour
{
    public event Action<BallBehaviour> onCollisionEvent;

    [SerializeField] private bool pushDownOnAwake;
    [SerializeField] private float pushDelay;
    [SerializeField] private float pushMult;
    [SerializeField] private float startingPushForce;
    [SerializeField] private float maxVelocityMagnitude;
    [SerializeField] private float damageRadius;
    [SerializeField] private LayerMask blockMask;
    [SerializeField] private int ballDamage;
    [SerializeField] private float wallCompensationForce;

    private Rigidbody2D rBody;
    private Vector2 startingPosition;
    public bool IsPlayerBall { get; private set; }

    public UnityEvent onCollision;



    public void Push(Vector2 velocity)
    {
        //GetComponent<Rigidbody2D>().AddForce(velocity * pushMult, ForceMode2D.Impulse);
    }

    public void Destroy()
    {
        transform.position = startingPosition;
        rBody.velocity = GetRandomDirection() * startingPushForce;
    }

    public void SetPlayerBall()
    {
        IsPlayerBall = true;
    }

    private void Awake()
    {
        if (pushDownOnAwake)
            Invoke("StartingPush", pushDelay);

        rBody = GetComponent<Rigidbody2D>();
        startingPosition = transform.position;
    }

    private void Update()
    {
        rBody.velocity = Vector2.ClampMagnitude(rBody.velocity, maxVelocityMagnitude);
    }

    private void StartingPush()
    {
        rBody.velocity = GetRandomDirection() * startingPushForce;
    }

    private Vector2 GetRandomDirection()
    {
        float randomAngle = UnityEngine.Random.Range(0, 360);
        Vector2 randomVector = Quaternion.Euler(0, 0, randomAngle) * Vector2.up;
        return randomVector;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Wall")
        {
            // if we hit a wall, add force to the opposite direction
            Vector2 fromWallDirection = transform.position - collision.transform.position;

            float direction = Mathf.Sign(fromWallDirection.x);

            Vector2 wallForce = new Vector2(direction * wallCompensationForce, 0);
            rBody.AddForce(wallForce);
        }

        BlockBehaviour block = collision.collider.GetComponent<BlockBehaviour>();

        if (block != null)
        {
            block.TakeDamage(ballDamage);
            onCollisionEvent?.Invoke(this);
        }

        onCollision?.Invoke();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, damageRadius);
    }
}