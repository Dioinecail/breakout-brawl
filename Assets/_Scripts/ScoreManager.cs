﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ScoreManager : MonoBehaviour
{
    public int scorePlayer;
    public int scoreCPU;

    public TMP_Text textScorePlayer;
    public TMP_Text textScoreCPU;

    public UnityEvent onScorePlayerUpdated;
    public UnityEvent onScoreCPUUpdated;




    private void OnBallCollidedWithBlock(BallBehaviour target)
    {
        if (target.IsPlayerBall)
            UpdateScorePlayer();
        else
            UpdateScoreCPU();
    }

    private void OnBallsSpawned(BallBehaviour[] balls)
    {
        for (int i = 0; i < balls.Length; i++)
        {
            balls[i].onCollisionEvent += OnBallCollidedWithBlock;
        }
    }

    private void OnEnable()
    {
        GameManager.onBallsSpawned += OnBallsSpawned;
    }

    private void OnDisable()
    {
        GameManager.onBallsSpawned -= OnBallsSpawned;
    }

    private void UpdateScorePlayer()
    {
        scorePlayer++;
        textScorePlayer.text = scorePlayer.ToString();
        onScorePlayerUpdated?.Invoke();
    }

    private void UpdateScoreCPU()
    {
        scoreCPU++;
        textScoreCPU.text = scoreCPU.ToString();
        onScoreCPUUpdated?.Invoke();
    }
}
