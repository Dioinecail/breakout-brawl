﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BlockBehaviour : MonoBehaviour
{
    public int startingHealth;
    public float damageCooldown;

    private int currentHealth;
    private Coroutine coroutine_Damage;

    public UnityEvent onTakeDamage;



    public void TakeDamage(int amount)
    {
        if (coroutine_Damage != null)
            return;

        coroutine_Damage = StartCoroutine(DamageCooldownCoroutine());

        currentHealth -= amount;

        GetComponent<SpriteRenderer>().color = Color.Lerp(Color.black, Color.white, Percent(currentHealth, startingHealth));

        onTakeDamage?.Invoke();

        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void Awake()
    {
        currentHealth = startingHealth;
    }

    private IEnumerator DamageCooldownCoroutine()
    {
        yield return new WaitForSeconds(damageCooldown);

        coroutine_Damage = null;
    }

    private float Percent(int current, int max)
    {
        return (float)current / max;
    }
}
