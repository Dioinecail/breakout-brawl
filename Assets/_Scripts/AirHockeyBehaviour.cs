﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirHockeyBehaviour : MonoBehaviour
{
    public float collisionRadius;
    public LayerMask collisionMask;
    public Vector2 restrictionAreaMin;
    public Vector2 restrictionAreaMax;

    private Collider2D[] buffer;
    private Vector2 lastPosition;
    private Vector2 deltaVelocity;
    private Camera mainCam;
    private Rigidbody2D rBody;



    private void Awake()
    {
        buffer = new Collider2D[16];
        mainCam = Camera.main;
        rBody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if(Input.GetMouseButton(0))
        {
            MoveToCursor();
        }

        ClearBuffer();
        CacheVelocity();
        DetectCollision();
    }

    private void ClearBuffer()
    {
        for (int i = 0; i < buffer.Length; i++)
        {
            buffer[i] = null;
        }
    }

    private void CacheVelocity()
    {
        deltaVelocity = (Vector2)transform.position - lastPosition;

        lastPosition = transform.position;
    }

    private void DetectCollision()
    {
        Physics2D.OverlapCircleNonAlloc(transform.position, collisionRadius, buffer, collisionMask);

        for (int i = 0; i < buffer.Length; i++)
        {
            if(buffer[i] != null)
            {
                BallBehaviour ball = buffer[i].GetComponent<BallBehaviour>();

                ball?.Push(deltaVelocity);
            }
        }
    }

    private void MoveToCursor()
    {
        Vector2 mousePos = Input.mousePosition;

        Vector2 worldPos = ApplyConstraints(mainCam.ScreenToWorldPoint(mousePos), restrictionAreaMin, restrictionAreaMax);

        rBody.MovePosition(worldPos);
    }

    private Vector2 ApplyConstraints(Vector2 target, Vector2 min, Vector2 max)
    {
        target.x = Mathf.Clamp(target.x, min.x, max.x);
        target.y = Mathf.Clamp(target.y, min.y, max.y);

        return target;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawWireSphere(transform.position, collisionRadius);

        Vector2 restrictionAreaCenter = restrictionAreaMin + (restrictionAreaMax - restrictionAreaMin) / 2;
        Vector2 restrictionAreaSize = restrictionAreaMax - restrictionAreaMin;

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(restrictionAreaCenter, restrictionAreaSize);
    }
}
